
/*


Sintaxis de JQuery:
===================

$(lo que vas a llamar).accion(proceso)


Ayuda Memoria:
==============


$(selector).action()

$(this).action -- es cuando ya habias llamado al elemento
 
$(".test").action -- Acá llama a una clase

$("img").css(background:) -- Llamando todos los img del proyecto y cambiando el background

$('#cantidad').html($('#formulario').val()*0.5);
*/

/*Sintaxis de funtiones
        function -nombre-(parametro a usar){
            aca adentro sucede el
        }
    */

//VARIABLES:    
const calculoB = 0.7;
const calculoM = 0.5;
const calculoP = 0.3;


//FUNCIONES:
function calcular(){
    let valor1 = $('#buenCalc').val();
    let valor2 = $('#modCalc').val();
    let valor3 = $('#pocoCalc').val();

    let resultado1 = valor1 * calculoB;
    let resultado2 = valor2 * calculoM;
    let resultado3 = valor3 * calculoP;

    totalCompraKG = resultado1 + resultado2 + resultado3;

    console.log(totalCompraKG);
    
    //Dibujo los resultados en html
    $('#resultado').html(totalCompraKG);
}



//PROGRAMA:
$(document).ready(function(){
    console.log("Pagina lista"); 
    
    $("#btnCalc").click(function(){
        console.log('se dio click');
        calcular();
    }
        
    );

});